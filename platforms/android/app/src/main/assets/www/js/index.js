/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    TAG: '[app]',
    steps: {
        'INIT': 'Initialization',
        'UPDATE': 'Update sources',
        'INSTALL': 'Install updated files'
    },
    exec: function(success, err, action, args){
        console.log(this.TAG, 'exec', action, args);
        window.cordova.exec(success, err, "SplashScreen", action, args);
    },
    hideSplash: function(success, error){
        console.log(this.TAG, 'hide');
        this.exec(success, error, "hide", []);
    },
    sendMessageToSplash: function(stepKey, stepMessage, error){
        var args = [ stepKey, this.steps[stepKey], stepMessage];
        console.log(this.TAG, 'add', args);
        this.exec(null, error, "add", args);
    },
    fakeBoot: function(){
        var self = this;
        var m = function(key, message, delay){
            return new Promise(function(resolve, reject){
                setTimeout(function(){
                    self.sendMessageToSplash(key, message, reject);
                    resolve();
                }, delay);
            });
        }
        var h = function(delay){
            return new Promise(function(resolve, reject){
                setTimeout(function(){
                    self.hideSplash(null, reject);
                    resolve();
                }, delay);
            });
        };

        var s = function(){
            return new Promise(function(resolve, reject){
                self.exec(resolve, reject, "show", []);
            });
        };

        return s()
            .then(m('INIT', 'Init files', 2000))
            .then(m('UPDATE', 'Checking updates', 6000))
            .then(m('INSTALL', 'Install updates', 5000))
            .then(m('INSTALL', 'Checking updates done', 3300))
            .then(m('INIT', 'Loading portal', 10000))
            .then(m('INSTALL', 'Copying files to device done', 6000))
            .then(m('UPDATE', 'Sources updated', 2000))
            .then(m('UPDATE', 'Raster updated', 3000))
            .then(h(10000))
            .catch(function(err){
                console.error(self.TAG, 'An error occured during boot', err);
            });
    },
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
	this.fakeBoot();
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();
