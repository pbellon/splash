package org.apache.cordova.splashscreen;

import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.ActionMode;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.cordova.CordovaActivity;
import org.apache.cordova.CordovaInterface;

public class SplashScreenDialog extends Dialog {
    private final static String TAG = "SplashScreenDialog";
    private int style = 0;
    SplashScreen plugin;
    /**
     * Remember last device orientation to detect orientation changes.
     */
    private int orientation;
    private View splashLayout;
    private ImageView splashImageView;
    private TextView splashTitleView;
    private TextView splashDescriptionView;
    private ProgressBar progressBar;

    private SplashScreenDialog(Context context) {
        super(context);
    }

    public SplashScreenDialog(SplashScreen plugin) {
        super(plugin.cordova.getActivity(), plugin.getSplashScreenStyleId());
        Log.d(TAG, String.format("style id: %s", plugin.getSplashScreenStyleId()));
        this.plugin = plugin;
    }

    private int getId(String name){
        return plugin.getResourceId(name, "id");
    }

    @Override
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        updateContentView();
    }

    public void setFullscreen(){
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
    }

    public void updateContentView(){
        int layoutId = plugin.getSplashScreenLayoutId();
        Log.d(TAG, String.format("layoutId: %s", layoutId));

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(layoutId);
        setFullscreen();

        splashLayout = findViewById(getId("splashLayoutRoot"));
        splashImageView = findViewById(getId("splashImageView"));
        splashTitleView = findViewById(getId("splashTitleView"));
        splashDescriptionView = findViewById(getId("splashDescriptionView"));
        progressBar = findViewById(getId("progressBar"));
    }

    public void updateSplashImageView(){
        if (splashImageView != null) {
            int drawableId = plugin.getSplashId();
            if (drawableId != 0) {
                splashImageView.setImageDrawable(plugin.cordova.getActivity().getResources().getDrawable(drawableId));
            }
        }
    }

    public void setMessage(String message){
        splashDescriptionView.setText(Html.fromHtml(message));
    }

    public void onOrientationChanged(Configuration newConfig) {
        // main change the layout (from port to landscape)
        updateContentView();
        // Splash drawable may change with orientation, so reload it.
        updateSplashImageView();
    }

    public void setAnimation(Animation animation) {
        splashLayout.setAnimation(animation);
        splashImageView.setAnimation(animation);

        splashLayout.startAnimation(animation);
        splashImageView.startAnimation(animation);
    }

    public void stopSpinner(){
        progressBar.setVisibility(View.GONE);
    }

    public void startSpinner(){
        progressBar.setIndeterminate(true);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            String colorName = plugin.getSpinnerColor();
            if(colorName != null){
                int[][] states = new int[][] {
                        new int[] { android.R.attr.state_enabled}, // enabled
                        new int[] {-android.R.attr.state_enabled}, // disabled
                        new int[] {-android.R.attr.state_checked}, // unchecked
                        new int[] { android.R.attr.state_pressed}  // pressed
                };
                int progressBarColor = Color.parseColor(colorName);
                int[] colors = new int[] {
                        progressBarColor,
                        progressBarColor,
                        progressBarColor,
                        progressBarColor
                };
                ColorStateList colorStateList = new ColorStateList(states, colors);
                progressBar.setIndeterminateTintList(colorStateList);
            }
        }
        progressBar.setVisibility(View.VISIBLE);
    }
}
