#!/usr/bin/env sh
ANDROID=../../platforms/android/app/src/main
ANDROID_SRC=$ANDROID/java/org/apache/cordova/splashscreen
ANDROID_LAYOUT=$ANDROID/res/layout
PLUGIN_SRC=src/android
PLUGIN_LAYOUT=$PLUGIN_SRC/res/layout

cp_src(){
    fn=$1
    cp $ANDROID_SRC/$fn.java $PLUGIN_SRC/$fn.java
    echo -e "\t\tCopied $fn.java\n\t\t- from $ANDROID_SRC\n\t\t- to $PLUGIN_SRC\n"
}

cp_layout(){
    fn=$1
    cp $ANDROID_LAYOUT/$fn.xml $PLUGIN_LAYOUT/$fn.xml
    echo -e "\t\tCopied $fn.xml\n\t\t- from $ANDROID_LAYOUT\n\t\t- to $PLUGIN_LAYOUT\n"
}

echo -e "\tUpdate plugin sources from application\n\n"

cp_src SplashScreen
cp_src SplashScreenDialog

cp_layout splashscreen
