#!/usr/bin/env sh
ANDROID=../../platforms/android/app/src/main
ANDROID_SRC=$ANDROID/java/org/apache/cordova/splashscreen
ANDROID_LAYOUT=$ANDROID/res/layout
ANDROID_JS=$ANDROID/assets/www/plugins/cordova-plugin-splashscreen/www

PLUGIN_SRC=src/android
PLUGIN_LAYOUT=$PLUGIN_SRC/res/layout
PLUGIN_JS=www

echo "js:"
echo $PLUGIN_JS
ls $PLUGIN_JS

cp_src(){
    fn=$1
    cp $PLUGIN_SRC/$fn.java $ANDROID_SRC/$fn.java
    echo -e "\t\tCopied $fn\n\t\t- from $PLUGIN_SRC\n\t\t- to $ANDROID_SRC\n"
}

cp_layout(){
    fn=$1
    cp $PLUGIN_LAYOUT/$fn.xml $ANDROID_LAYOUT/$fn.xml
    echo -e "\t\tCopied $fn.xml\n\t\t- from $PLUGIN_LAYOUT\n\t\t- to $ANDROID_LAYOUT\n"
}

cp_js(){
    fn=$1.js
    content=$(cat $PLUGIN_JS/$fn)
    ajs=$ANDROID_JS/$fn
    cat >$ajs <<EOL
cordova.define("cordova-plugin-splashscreen.SplashScreen", function(require, exports, module) {
${content}
});
EOL
    echo -e "\t\tWritten new js in $ANDROID_JS/$fn\n"
    cat $ajs
}


echo -e "\tUpdate app sources from plugin\n\n"

cp_src SplashScreen
cp_src SplashScreenDialog
cp_layout splashscreen

cp_js splashscreen
