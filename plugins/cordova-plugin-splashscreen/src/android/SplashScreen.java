/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/

package org.apache.cordova.splashscreen;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


class BootStepMessage {
    public String stepKey;
    public String stepTitle;
    public String message;

    public BootStepMessage(String stepKey, String stepTitle, String message) {
        this.stepKey = stepKey;
        this.stepTitle = stepTitle;
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format("BootStepMessage(stepKey=%s, stepTile=%s, message=%s)", stepKey, stepTitle, message);
    }
}
class BootStep {
    public String title;
    public String key;
    private ArrayList<BootStepMessage> messages;

    public BootStep(String key, String title){
        this.key = key;
        this.title = title;
        this.messages = new ArrayList<>();
    }

    public void addMessage(BootStepMessage message){
        this.messages.add(message);
    }

    public ArrayList<BootStepMessage> listMessages(){
        return this.messages;
    }

}
class SplashMessageBuilder {
    private final static String TAG = "SplashMessageBuilder";

    private SplashScreen plugin;
    private HashMap<String, BootStep> steps;

    static public SplashMessageBuilder init(SplashScreen plugin){
        return new SplashMessageBuilder(plugin);
    }

    private SplashMessageBuilder(SplashScreen plugin){
        this.plugin = plugin;
        this.steps = new HashMap<String, BootStep>();
    }

    private BootStep getOrCreate(BootStepMessage message){
        if(this.steps.containsKey(message.stepKey)) {
            return this.steps.get(message.stepKey);
        } else {
            BootStep step = new BootStep(message.stepKey, message.stepTitle);
            this.steps.put(message.stepKey, step);
            return step;
        }
    }

    public void addMessage(BootStepMessage message){
        Log.d(TAG, String.format("addMessage(%s)", message));
        BootStep step = getOrCreate(message);
        step.addMessage(message);
    }

    public String build(){
        StringBuilder builder = new StringBuilder();
        for (BootStep step : this.steps.values()){
            builder.append(String.format("<b><span style=\"size: 2\">%s</span></b><br>\n", step.title));
            for (BootStepMessage message : step.listMessages()) {
                builder.append(String.format("\n\t<span style=\"size: 1.5\">%s</span><br/>\n", message.message));
            }
            builder.append("<br>");
        }
        return builder.toString();
    }

    public BootStepMessage buildMessage(String bootStepKey, String bootStepTitle, String bootStepMessage){
        return new BootStepMessage(bootStepKey, bootStepTitle, bootStepMessage);
    }


}

public class SplashScreen extends CordovaPlugin {

    private static SplashMessageBuilder splashMessageBuilder;
    // test modif
    private static final String LOG_TAG = "SplashScreen";
    // Cordova 3.x.x has a copy of this plugin bundled with it (SplashScreenInternal.java).
    // Enable functionality only if running on 4.x.x.
    private static final boolean HAS_BUILT_IN_SPLASH_SCREEN = Integer.valueOf(CordovaWebView.CORDOVA_VERSION.split("\\.")[0]) < 4;
    private static final int DEFAULT_SPLASHSCREEN_DURATION = 10000;
    private static final int DEFAULT_FADE_DURATION = 1000;
    private static final String TAG = "SplashScreen";
    /*
    private static Dialog splashScreen;
    private static ProgressDialog spinnerDialog;
    */

    private static SplashScreenDialog splashScreen;
    private static boolean firstShow = true;
    private static boolean lastHideAfterDelay; // https://issues.apache.org/jira/browse/CB-9094

    /**
     * Displays the splash drawable.
     */
    private ImageView splashImageView;

    /**
     * Remember last device orientation to detect orientation changes.
     */
    private int orientation;

    // Helper to be compile-time compatible with both Cordova 3.x and 4.x.
    private View getView() {
        try {
            return (View)webView.getClass().getMethod("getView").invoke(webView);
        } catch (Exception e) {
            return (View)webView;
        }
    }

    protected int getResourceId(String name, String defType){
        int resourceId = 0;
        if (name != null) {
            Resources res = cordova.getActivity().getResources();
            resourceId = res.getIdentifier(name, defType, cordova.getActivity().getClass().getPackage().getName());
            if (resourceId == 0) {
                resourceId = res.getIdentifier(name, defType, cordova.getActivity().getPackageName());
            }
        }
        return resourceId;
    }

    protected int getSplashScreenStyleId(){
        return getResourceId(preferences.getString("SplashScreenStyle", "splashScreenStyle"), "style");
    }

    protected int getSplashId() {
        String splashResource = preferences.getString("SplashScreen", "screen");
        return getResourceId(splashResource, "drawable");
    }

    protected int getSplashScreenLayoutId() {
        return getResourceId(preferences.getString("SplashScreenLayout", "splashscreen"), "layout");
    }
    protected Resources getResources(){
        return cordova.getActivity().getResources();
    }

    protected String getSpinnerColor(){
        return preferences.getString("SplashScreenSpinnerColor", null);
    }

    private void initializeMessageBuilder(){
        if (splashMessageBuilder == null) {
            splashMessageBuilder = SplashMessageBuilder.init(this);
        }
    }
    @Override
    protected void pluginInitialize() {
        if (HAS_BUILT_IN_SPLASH_SCREEN) {
            return;
        }
        initializeMessageBuilder();
        // Make WebView invisible while loading URL
        // CB-11326 Ensure we're calling this on UI thread
        cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getView().setVisibility(View.INVISIBLE);
            }
        });
        int drawableId = getSplashId();

        // Save initial orientation.
        orientation = cordova.getActivity().getResources().getConfiguration().orientation;

        if (firstShow) {
            boolean autoHide = preferences.getBoolean("AutoHideSplashScreen", true);
            Log.d(TAG, String.format("autoHide ? %s", autoHide));
            showSplashScreen(autoHide);
        }

        if (preferences.getBoolean("SplashShowOnlyFirstTime", true)) {
            firstShow = false;
        }
    }

    /**
     * Shorter way to check value of "SplashMaintainAspectRatio" preference.
     */
    private boolean isMaintainAspectRatio () {
        return preferences.getBoolean("SplashMaintainAspectRatio", false);
    }

    private int getFadeDuration () {
        int fadeSplashScreenDuration = preferences.getBoolean("FadeSplashScreen", true) ?
            preferences.getInteger("FadeSplashScreenDuration", DEFAULT_FADE_DURATION) : 0;

        if (fadeSplashScreenDuration < 30) {
            // [CB-9750] This value used to be in decimal seconds, so we will assume that if someone specifies 10
            // they mean 10 seconds, and not the meaningless 10ms
            fadeSplashScreenDuration *= 1000;
        }

        return fadeSplashScreenDuration;
    }

    @Override
    public void onPause(boolean multitasking) {
        if (HAS_BUILT_IN_SPLASH_SCREEN) {
            return;
        }
        // hide the splash screen to avoid leaking a window
        this.removeSplashScreen(true);
    }

    @Override
    public void onDestroy() {
        if (HAS_BUILT_IN_SPLASH_SCREEN) {
            return;
        }
        // hide the splash screen to avoid leaking a window
        this.removeSplashScreen(true);
        // If we set this to true onDestroy, we lose track when we go from page to page!
        //firstShow = true;
    }

    static public JSONObject buildArgs(String action, JSONArray args) throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("action", action);
        obj.put("args", args);
        return obj;
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        String[] actions = {"show", "hide", "add"};
        if (Arrays.asList(action).contains(action)){
            final String data = buildArgs(action, args).toString();
            cordova.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    webView.postMessage("splashscreen", data);
                }
            });
            callbackContext.success();
        } else {
            return false;
        }
        return true;
    }

    public void addMessage(JSONArray args) throws JSONException {
        String stepKey = args.getString(0);
        String stepTitle = args.getString(1);
        String stepMessage = args.getString(2);
        splashMessageBuilder.addMessage(splashMessageBuilder.buildMessage(stepKey, stepTitle, stepMessage));
        splashScreen.setMessage(splashMessageBuilder.build());
    }

    @Override
    public Object onMessage(String id, Object data) {
        if (HAS_BUILT_IN_SPLASH_SCREEN) {
            return null;
        }
        if ("splashscreen".equals(id)) {
            JSONObject message = null;
            try {
                message = new JSONObject(data.toString());
                String action = message.getString("action");
                JSONArray args = message.getJSONArray("args");
                Log.d(TAG, String.format( "onMessage(%s)", data));
                if ("hide".equals(action)) {
                    this.removeSplashScreen(false);
                } else if ("show".equals(action)) {
                    this.showSplashScreen(false);
                } else if ("add".equals(action)){
                    this.addMessage(args);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if ("spinner".equals(id)) {
            if ("stop".equals(data.toString())) {
                getView().setVisibility(View.VISIBLE);
            }
        } else if ("onReceivedError".equals(id)) {
            this.hide();
        }
        return null;
    }

    // Don't add @Override so that plugin still compiles on 3.x.x for a while
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation != orientation) {
            orientation = newConfig.orientation;


            // Splash drawable may change with orientation, so reload it.
            splashScreen.onOrientationChanged(newConfig);

            if (splashImageView != null) {
                int drawableId = getSplashId();
                if (drawableId != 0) {
                    splashImageView.setImageDrawable(cordova.getActivity().getResources().getDrawable(drawableId));
                }
            }
        }
    }

    private void removeSplashScreen(final boolean forceHideImmediately) {
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (splashScreen != null && splashScreen.isShowing()) {
                    final int fadeSplashScreenDuration = getFadeDuration();
                    // CB-10692 If the plugin is being paused/destroyed, skip the fading and hide it immediately
                    if (fadeSplashScreenDuration > 0 && forceHideImmediately == false) {
                        AlphaAnimation fadeOut = new AlphaAnimation(1, 0);
                        fadeOut.setInterpolator(new DecelerateInterpolator());
                        fadeOut.setDuration(fadeSplashScreenDuration);
                        splashScreen.setAnimation(fadeOut);
                        fadeOut.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                hide();
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                if (splashScreen != null && splashScreen.isShowing()) {
                                    splashScreen.dismiss();
                                    splashScreen = null;
                                }
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });
                    } else {
                        hide();
                        splashScreen.dismiss();
                    }
                }
            }
        });
    }

    /**
     * Shows the splash screen over the full Activity
     */
    @SuppressWarnings("deprecation")
    private void showSplashScreen(final boolean hideAfterDelay) {
        SplashScreen plugin = this;
        final int splashscreenTime = preferences.getInteger("SplashScreenDelay", DEFAULT_SPLASHSCREEN_DURATION);

        final int fadeSplashScreenDuration = getFadeDuration();
        final int effectiveSplashDuration = Math.max(0, splashscreenTime - fadeSplashScreenDuration);

        lastHideAfterDelay = hideAfterDelay;

        // Prevent to show the splash dialog if the activity is in the process of finishing
        if (cordova.getActivity().isFinishing()) {
            return;
        }
        // If the splash dialog is showing don't try to show it again
        if (splashScreen != null && splashScreen.isShowing()) {
            return;
        }
        if (splashscreenTime <= 0 && hideAfterDelay) {
            return;
        }

        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                // Get reference to display
                Display display = cordova.getActivity().getWindowManager().getDefaultDisplay();
                Context context = webView.getContext();
                // Create and show the dialog
                splashScreen = new SplashScreenDialog(plugin);
                splashScreen.setCancelable(false);
                splashScreen.show();

                if (preferences.getBoolean("ShowSplashScreenSpinner", true)) {
                    show();
                }

                // Set Runnable to remove splash screen just in case
                if (hideAfterDelay) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (lastHideAfterDelay) {
                                removeSplashScreen(false);
                            }
                        }
                    }, effectiveSplashDuration);
                }
            }
        });
    }

    // Show only spinner in the center of the screen
    private void show() {
        SplashScreen plugin = this;
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (splashScreen == null) {
                    splashScreen = new SplashScreenDialog(plugin);
                }

                splashScreen.stopSpinner();
                splashScreen.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        splashScreen = null;
                    }
                });

                splashScreen.setCancelable(false);
                splashScreen.startSpinner();
                splashScreen.show();
            }
        });
    }

    private void hide() {
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (splashScreen != null && splashScreen.isShowing()) {
                    splashScreen.dismiss();
                    splashScreen = null;
                }
            }
        });
    }
}
